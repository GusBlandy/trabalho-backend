const { response } = require('express');
const Address = require('../../models/Address');
const Client = require('../../models/Client');

const create = async(req, res) => {
    try {
        const client = await Client.create(req.body);
        return res.status(200).json({message: "Cliente cadastrado com sucesso!", client: client});
    } catch(err) {
        return res.status(500).json({error: err});
    }
};

const show = async(req,res) => {
    const {id} = req.params;
    try {
        const client = await Client.findByPk(id);
        return res.status(200).json({client});
    }catch(err){
        return res.status(500).json({err});
    }
};

const index = async(req, res) => {
    try {
        const clients = await Client.findAll();
        return res.status(200).json({clients})
    } catch (err) {
        return res.status(500).json({error: err});
    }
};

const update = async(req, res) => {
    const {id} = req.params;
    try {
        const [updated] = await Client.update(req.body, {where: {id:id}});
        if (updated) {
            const client = await Client.findByPk(id);
            return res.status(200).send(client);
        }
        throw new Error();
    } catch(err) {
        return res.status(500).json({error: err});
    }
};

const destroy = async(req, res) => {
    const {id} = req.params;
    try {
        const deleted = await Client.destroy({where: {id:id}});
        if (deleted) {
            return res.status(200).json("Cliente deletado com sucesso.");
        }
        throw new Error ();
    } catch(err) {
        return res.status(500).json("Cliente não encontrado");
    }
};

const addHome = async(req,res) => {
    const {id} = req.params;
    try {
        const address = await Address.findByPk(id);
        const client = await Client.findByPk(req.body.ClientId);
        await address.setSeller(client);
        return res.status(200).json(address);
    }catch(err){
        return res.status(500).json({err});
    }
};

const removeHome = async(req,res) => {
    const {id} = req.params;
    try {
        const address = await Address.findByPk(id);
        await address.setRole(null);
        return res.status(200).json(address);
    }catch(err){
        return res.status(500).json({err});
    }
};

module.exports = {
    index,
    show,
    create,
    update,
    destroy,
    addHome,
    removeHome
};
