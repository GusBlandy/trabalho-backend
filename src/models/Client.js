const Datatypes = require('sequelize');
const sequelize = require('../config/sequelize');

const Client = sequelize.define('Client', {
    name: {
        type: Datatypes.STRING,
        allowNull: false
    },
    CPF: {
        type: Datatypes.STRING,
        allowNull: false
    },
    phone_number: {
        type: Datatypes.STRING,
        allowNull: false
    },
    payment_method: {
        type: Datatypes.STRING,
        allowNull: false
    },
    email: {
        type: Datatypes.STRING,
        allowNull: false
    },
    date_of_birthday: {
        type: Datatypes.DATEONLY,
        allowNull: false
    },
});


Client.associate = function (models) {
    Client.belongsTo(models.Address)
    Client.belongsToMany(models.Product, {through: 'cart'})
};


module.exports = Client;
