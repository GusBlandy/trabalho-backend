const Datatypes = require('sequelize');
const sequelize = require('../config/sequelize');

const Product = sequelize.define('Product', {
    name: {
        type: Datatypes.STRING,
        allowNull: false
    },
    description: {
        type: Datatypes.TEXT,
        allowNull: false
    },
    color: {
        type: Datatypes.STRING,
        allowNull: false
    },
    price: {
        type: Datatypes.DOUBLE,
        allowNull: false
    },
    photograph: {
        type: Datatypes.TEXT,
        allowNull: false
    },
    inventory: {
        type: Datatypes.INTEGER,
        allowNull: false
    },
});


Product.associate = function (models) {
    Product.belongsTo(models.Seller)
    Product.belongsToMany(models.Client, {through: 'cart'})

};

module.exports = Product;
