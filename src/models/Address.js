const Datatypes = require('sequelize');
const sequelize = require('../config/sequelize');

const Address = sequelize.define('Address', {
    country: {
        type: Datatypes.STRING,
        allowNull: false
    },
    state: {
        type: Datatypes.STRING,
        allowNull: false
    },
    city: {
        type: Datatypes.STRING,
        allowNull: false
    },
    district: {
        type: Datatypes.STRING,
        allowNull: false
    },
    road: {
        type: Datatypes.STRING,
        allowNull: false
    },
    number: {
        type: Datatypes.INTEGER,
        allowNull: false
    },
});


Address.associate = function (models) {
    Address.hasMany(models.Client)
};


module.exports = Address;
