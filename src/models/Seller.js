const Datatypes = require('sequelize');
const sequelize = require('../config/sequelize');

const Seller = sequelize.define('Seller', {
    name: {
        type: Datatypes.STRING,
        allowNull: false
    },
    address: {
        type: Datatypes.STRING,
        allowNull: false
    },
    phone_number: {
        type: Datatypes.STRING,
        allowNull: false
    },
    type: {
        type: Datatypes.STRING,
        allowNull: false
    },
    email: {
        type: Datatypes.STRING,
        allowNull: false
    },
    date_of_birthday: {
        type: Datatypes.DATEONLY,
        allowNull: false
    },
    CPF: {
        type: Datatypes.STRING,
        allowNull: false
    }
});


Seller.associate = function (models) {
    Seller.hasMany(models.Product)
};


module.exports = Seller;
