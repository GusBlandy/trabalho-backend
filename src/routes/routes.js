const { Router } = require('express');
const router = Router();
const AddressController = require('../controllers/AddressController/AddressController');
const ClientController = require('../controllers/ClientController/ClientController');
const ProductController = require('../controllers/ProductController/ProductController');
const SellerController = require('../controllers/SellerController/SellerController');

router.get('/addresses',AddressController.index);
router.get('/addresses/:id',AddressController.show);
router.post('/addresses',AddressController.create);
router.put('/addresses/:id', AddressController.update);
router.delete('/addresses/:id', AddressController.destroy);

router.get('/clients',ClientController.index);
router.get('/clients/:id',ClientController.show);
router.post('/clients',ClientController.create);
router.put('/clients/:id', ClientController.update);
router.delete('/clients/:id', ClientController.destroy);
router.put('/clients/:id', ClientController.addHome);
router.put('/clients/:id', ClientController.removeHome);

router.get('/products',ProductController.index);
router.get('/products/:id',ProductController.show);
router.post('/products',ProductController.create);
router.put('/products/:id', ProductController.update);
router.delete('/products/:id', ProductController.destroy);
router.put('/products/:id', ProductController.postingProduct);
router.put('/products/:id', ProductController.removePost);

router.get('/sellers',SellerController.index);
router.get('/sellers/:id',SellerController.show);
router.post('/sellers',SellerController.create);
router.put('/sellers/:id', SellerController.update);
router.delete('/sellers/:id', SellerController.destroy);


module.exports = router;
